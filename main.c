#include <stdio.h>
void callByValue(int i);
void callByPointer(int * i);
int main(){
  int source = 10;
  callByValue(source);
  int source2 = 10;
  callByPointer(&source2);

  printf("%d\n", source);
  printf("%d\n", source2);

}

void callByValue(int i){
  ++i;
}

void callByPointer(int * i){
  *i += 1;
}
