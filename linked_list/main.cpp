#include <iostream>

struct node{
    int value;
    node * next;
};


void print_linked_list(node * head){
    node * current = head;

    while(current != nullptr){
        std::cout << current->value << std::endl;
        current = current->next;
    }
}

void push(node ** head, int val){
    node * current = *head;
    node *new_node = new node;
    new_node->value = val;
    new_node->next = nullptr;
    if(*head == nullptr){
        *head = new_node;
        return;
    }
    while(current->next != nullptr){
        current = current->next;
    }
    current->next = new_node;
    return;
}


void clear(node * head){
    node * current = head;

    while(current != nullptr){
        node * previous = current;
        current = current->next;
        delete previous;
    }
}

int main() {
    node * head = new node;
    head->value = 10;
    head->next = nullptr;
    push(&head, 10);
    push(&head, 20);

    print_linked_list(head);
    clear(head);
    if(head != nullptr){
        delete head;
    }
}