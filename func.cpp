#include <iostream>
void callByReference(int& i);
int main(){
  int source = 10;
  callByReference(source);
  std::cout << source << std::endl;

}

void callByReference(int& i){
  i += 10;
}
