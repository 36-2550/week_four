//
// Created by karl on 2/13/18.
//

#include "piece.h"
#include "board.h"

#ifndef CHESS_ONE_BOARD_FACTORY_H
#define CHESS_ONE_BOARD_FACTORY_H


class board_factory {
public:
    board create();
};


#endif //CHESS_ONE_BOARD_FACTORY_H
