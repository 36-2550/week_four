//
// Created by karl on 2/13/18.
//

#ifndef CHESS_ONE_PIECE_H
#define CHESS_ONE_PIECE_H
enum piece_type {none, pawn, rook, knight, queen, bishop, king};
enum piece_color {black, white};
struct piece{
    piece_type type;
    piece_color color;
    bool is_taken;
    bool is_queen;
};
void take_piece(piece * piece);
void make_queen(piece * piece);

#endif //CHESS_ONE_PIECE_H
