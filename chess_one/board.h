//
// Created by karl on 2/13/18.
//

#include "piece.h"
#ifndef CHESS_ONE_BOARD_H
#define CHESS_ONE_BOARD_H

struct space{
    bool is_occupied;
    piece occupying_piece;
};

struct check_result{
    bool is_check;
    bool is_checkmate;
    piece_color attacking_color;
};

struct board{
    space board[8][8];
};

check_result check_if_check(board * board);
check_result check_if_checkmate(board * board);
#endif //CHESS_ONE_BOARD_H
